package net.Indyuce.mmocore.tree.skilltree;

public enum SkillTreeType {
    AUTOMATIC_SKILL_TREE(),
    LINKED_SKILL_TREE,
    CUSTOM_SKILL_TREE;
}
